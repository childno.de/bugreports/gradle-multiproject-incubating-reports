# the setup
* using a gradle multiproject
* using gradle wrapper (but doesn't matter)
* gradle version 3.3 up to 3.5 (doesn't matter which one)
* using [`allprojects` definition in root `build.gradle`](build.gradle) for common subproject configurations


# what works
see ([`build.gradle` line 9+ @83fc313b](https://gitlab.com/bugreports.childno.de/gradle-multiproject-incubating-reports/blob/83fc313bb08aa2bfb05d2972ff9a7bea655f10a6/build.gradle#L9)) 

* defining some values on some native but incubating help report tasks `dependentComponents`, `components` and `dependencyInsight`
 * https://docs.gradle.org/current/dsl/org.gradle.api.reporting.dependents.DependentComponentsReport.html
 * https://docs.gradle.org/current/dsl/org.gradle.api.reporting.components.ComponentReport.html
 * https://docs.gradle.org/current/dsl/org.gradle.api.tasks.diagnostics.DependencyInsightReportTask.html


# what doesn't work
see ([apply external file in `build.gradle` @410eca01](https://gitlab.com/bugreports.childno.de/gradle-multiproject-incubating-reports/blob/410eca01369a29cbbf0d7b835effa7cd0f704a44/build.gradle#L10)) 

see ([configure all in `reports.gradle` @410eca01](https://gitlab.com/bugreports.childno.de/gradle-multiproject-incubating-reports/blob/410eca01369a29cbbf0d7b835effa7cd0f704a44/reports.gradle)) 

```sh
bash:$ ./gradlew 

FAILURE: Build failed with an exception.

* Where:
Script '/Users/marcel/Projects/gradleBugReport/reports.gradle' line: 2

* What went wrong:
A problem occurred evaluating script.
> Could not find method dependentComponents() for arguments [reports_aupea0bbxx745fczdu9poi7rt$_run_closure1@54a01c43] on project ':subPrj' of type org.gradle.api.Project.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

BUILD FAILED

```


# what works - again
see ([apply external file in `build.gradle` @cd689d17](https://gitlab.com/bugreports.childno.de/gradle-multiproject-incubating-reports/blob/cd689d174637db9a5750235dd69629b45d2dfa44/build.gradle#L10)) 

see ([configure all in `reports.gradle` @cd689d17](https://gitlab.com/bugreports.childno.de/gradle-multiproject-incubating-reports/blob/cd689d174637db9a5750235dd69629b45d2dfa44/reports.gradle)) 

wrapping the definitions in the applied `reports.gradle` into `gradle.projectsEvaluated { }` make it work again

# the bug report
https://github.com/gradle/gradle/issues/1948